//
//  IDMyScene.h
//  Airplanes
//

//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <CoreMotion/CoreMotion.h>
#import "IDAirplane.h"
#import "IDGenerator.h"
#import "IDCollisionManager.h"

@interface IDMyScene : SKScene<IDGeneratorDelegate> {
    CGRect screenRect;
    CGFloat screenHeight;
    CGFloat screenWidth;
    
    double currentMaxAccelerationX;
    double currentMaxAccelerationY;
}

@property (nonatomic, strong) IDAirplane *player;
@property (nonatomic, strong) CMMotionManager *motionManager;

@end
