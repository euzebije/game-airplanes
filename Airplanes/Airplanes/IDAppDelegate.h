//
//  IDAppDelegate.h
//  Airplanes
//
//  Created by Ivan Fabijanovic on 10/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
