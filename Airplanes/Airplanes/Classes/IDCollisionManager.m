//
//  IDCollisionManager.m
//  Airplanes
//
//  Created by Ivan Fabijanovic on 11/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import "IDCollisionManager.h"

@implementation IDCollisionManager

#pragma mark - SKPhysicsContactDelegate

- (void)didBeginContact:(SKPhysicsContact *)contact {
    SKSpriteNode *plane;
    SKSpriteNode *projectile;
    if ([contact.bodyA.node isKindOfClass:[IDAirplane class]]) {
        plane = (SKSpriteNode *)contact.bodyA.node;
        projectile = (SKSpriteNode *)contact.bodyB.node;
    } else {
        plane = (SKSpriteNode *)contact.bodyB.node;
        projectile = (SKSpriteNode *)contact.bodyA.node;
    }
    
    if ([self isEnemyHit:contact]) {
        [projectile removeFromParent];
        if ([plane respondsToSelector:@selector(explode)]) {
            [plane performSelector:@selector(explode)];
        }
    } else if ([self isPlayerHit:contact]) {
        // Nothing for now
    }
}

#pragma mark - Private methods

- (BOOL)isPlayerHit:(SKPhysicsContact *)contact {
    return [self isPlane:IDCollisionBodyPlayer hitByProjectile:IDCollisionBodyEnemyProjectile inContact:contact];
}

- (BOOL)isEnemyHit:(SKPhysicsContact *)contact {
    return [self isPlane:IDCollisionBodyEnemy hitByProjectile:IDCollisionBodyPlayerProjectile inContact:contact];
}

- (BOOL)isPlane:(IDCollisionBody)planeMask hitByProjectile:(IDCollisionBody)projectileMask inContact:(SKPhysicsContact *)contact {
    SKPhysicsBody *firstBody;
    SKPhysicsBody *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    BOOL isCorrectProjectile = (firstBody.categoryBitMask & projectileMask) != 0;
    BOOL isCorrectPlane = (secondBody.categoryBitMask & planeMask) != 0;
    
    return isCorrectProjectile && isCorrectPlane;
}

@end
