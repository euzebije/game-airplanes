//
//  IDAirplane.h
//  Airplanes
//
//  Created by Ivan Fabijanovic on 10/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    IDAirplaneTurnLeft,
    IDAirplaneTurnNormal,
    IDAirplaneTurnRight
} IDAirplaneTurnDirection;

typedef enum {
    IDAirplaneModel1 = 1,
    IDAirplaneModel2 = 2,
    IDAirplaneModel8 = 8
} IDAirplaneModel;

@interface IDAirplane : SKNode

- (id)init;
- (id)initWithModel:(IDAirplaneModel)model;

- (void)turn:(IDAirplaneTurnDirection)direction;
- (SKSpriteNode *)fire;
- (void)explode;

@property (nonatomic, readonly, assign) CGSize size;
@property (nonatomic, readonly, assign) CGFloat width;
@property (nonatomic, readonly, assign) CGFloat height;
@property (nonatomic, readonly, assign) IDAirplaneModel model;

@property (nonatomic, readonly, assign) BOOL hasShadow;
@property (nonatomic, readonly, assign) BOOL canTurn;

@property (nonatomic, assign) BOOL isPlayer;

@end
