//
//  IDCollisionManager.h
//  Airplanes
//
//  Created by Ivan Fabijanovic on 11/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "IDAirplane.h"

@interface IDCollisionManager : NSObject<SKPhysicsContactDelegate>

@property (nonatomic, strong) NSMutableArray *explosionTextures;

@end
