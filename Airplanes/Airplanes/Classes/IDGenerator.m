//
//  IDGenerator.m
//  Airplanes
//
//  Created by Ivan Fabijanovic on 11/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import "IDGenerator.h"

#define kLoopActionName     @"IDGeneratorMainLoop"

@implementation IDGenerator {
    CGRect screenRect;
    CGFloat screenHeight;
    CGFloat screenWidth;
    
    NSArray *_cloudsTextures;
}

#pragma mark - Constructors

- (id)init {
    self = [super init];
    if (self) {
        self.interval = 1.0;
        self.generateEnemies = YES;
        
        screenRect = [[UIScreen mainScreen] bounds];
        screenHeight = screenRect.size.height;
        screenWidth = screenRect.size.width;
        
        // Clouds
        SKTextureAtlas *cloudsAtlas = [SKTextureAtlas atlasNamed:@"Clouds"];
        NSArray *textureNamesClouds = [cloudsAtlas textureNames];
        NSMutableArray *cloudsTextures = [[NSMutableArray alloc] initWithCapacity:textureNamesClouds.count];
        for (NSString *name in textureNamesClouds) {
            SKTexture *texture = [cloudsAtlas textureNamed:name];
            [cloudsTextures addObject:texture];
        }
        _cloudsTextures = cloudsTextures;
    }
    return self;
}

#pragma mark - Public methods

- (void)start {
    SKAction *wait = [SKAction waitForDuration:_interval];
    SKAction *generate = [SKAction runBlock:^{
        [self generateEnemy];
        [self generateCloud];
    }];
    SKAction *sequence = [SKAction sequence:@[wait, generate]];
    SKAction *loop = [SKAction repeatActionForever:sequence];
    [self.delegate runAction:loop withKey:kLoopActionName];
}

- (void)stop {
    [self.delegate removeActionForKey:kLoopActionName];
}

#pragma mark - Private methods

- (BOOL)generateEnemy {
    BOOL willCreateEnemy = [self getRandomNumberBetween:0 to:1];
    if (willCreateEnemy) {
        // Create an enemy plane with a random model
        IDAirplaneModel randomModel = (IDAirplaneModel)[self getRandomNumberBetween:1 to:2];
        IDAirplane *enemy = [[IDAirplane alloc] initWithModel:randomModel];
        enemy.scale = 0.6;
        enemy.zPosition = 1;
        [self.delegate addChild:enemy];
        
        // Generate a random path for the enemy plane to move on
        CGPathRef path = [self generateEnemyPath];
        NSTimeInterval duration = [self getRandomNumberBetween:2 to:10];

        // Enemy action sequence - move -> remove
        SKAction *move = [SKAction followPath:path asOffset:NO orientToPath:YES duration:duration];
        SKAction *remove = [SKAction removeFromParent];
        [enemy runAction:[SKAction sequence:@[move, remove]] withKey:kEnemyPlaneMovementAction];
        
        CGPathRelease(path);
    }
    return willCreateEnemy;
}

- (CGPathRef)generateEnemyPath {
    CGMutablePathRef path = CGPathCreateMutable();
    
    float startX = [self getRandomNumberBetween:0 to:screenWidth];
    float endX = [self getRandomNumberBetween:0 to:screenWidth];
    
    float point1X = [self getRandomNumberBetween:0 to:screenWidth];
    float point1Y = [self getRandomNumberBetween:0 to:screenHeight];
    
    float point2X = [self getRandomNumberBetween:0 to:screenWidth];
    float point2Y = [self getRandomNumberBetween:point1Y to:screenHeight];
    
    CGPoint start = CGPointMake(startX, screenHeight);
    CGPoint end = CGPointMake(endX, 0);
    CGPoint point1 = CGPointMake(point1X, point1Y);
    CGPoint point2 = CGPointMake(point2X, point2Y);
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddCurveToPoint(path, NULL, point1.x, point1.y, point2.x, point2.y, end.x, end.y);
    
    return path;
}

- (BOOL)generateCloud {
    BOOL willCreateCloud = [self getRandomNumberBetween:0 to:1];
    if (willCreateCloud) {
        int randomTexture = [self getRandomNumberBetween:0 to:3];
        int randomX = [self getRandomNumberBetween:0 to:screenWidth];
        SKSpriteNode *cloud = [SKSpriteNode spriteNodeWithTexture:[_cloudsTextures objectAtIndex:randomTexture]];
        cloud.position = CGPointMake(randomX, screenHeight + cloud.size.height);
        cloud.zPosition = 1;
        [self.delegate addChild:cloud];
        
        NSTimeInterval duration = [self getRandomNumberBetween:9 to:19];
        SKAction *move = [SKAction moveTo:CGPointMake(randomX, -cloud.size.height) duration:duration];
        SKAction *remove = [SKAction removeFromParent];
        [cloud runAction:[SKAction sequence:@[move, remove]]];
    }
    return willCreateCloud;
}

- (int)getRandomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to - from + 1);
}

@end
