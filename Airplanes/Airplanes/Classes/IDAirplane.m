//
//  IDAirplane.m
//  Airplanes
//
//  Created by Ivan Fabijanovic on 10/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import "IDAirplane.h"

#define kImageNormal        @"N"
#define kImageLeft          @"L"
#define kImageRight         @"R"
#define kImageShadow        @"SHADOW"
#define kImagePropeller1    @"PLANE_PROPELLER_1.png"
#define kImagePropeller2    @"PLANE_PROPELLER_2.png"
#define kImageProjectile    @"B_2.png"

#define kShadowOffsetX      15.0
#define kShadowOffsetY      -15.0
#define kPropellerOffsetY   _height/2.0
#define kSmokeTrailOffsetY  -_height/2.0

@interface IDAirplane()

@property (nonatomic, strong) SKSpriteNode *plane;
@property (nonatomic, strong) SKSpriteNode *planeShadow;
@property (nonatomic, strong) SKSpriteNode *propeller;
@property (nonatomic, strong) SKEmitterNode *smokeTrail;

@end

@implementation IDAirplane {
    NSString *_planeImageFormat;
}

#pragma mark - Constructors

- (id)init {
    self = [super init];
    if (self) {
        [self setupWithModel:IDAirplaneModel8];
        self.isPlayer = NO;
    }
    return self;
}

- (id)initWithModel:(IDAirplaneModel)model {
    self = [super init];
    if (self) {
        [self setupWithModel:model];
        self.isPlayer = NO;
    }
    return self;
}

- (void)setupWithModel:(IDAirplaneModel)model {
    _model = model;
    _planeImageFormat = [self imageFromModel:model];
    NSString *planeImage = [NSString stringWithFormat:_planeImageFormat, kImageNormal];
    NSString *shadowImage = [NSString stringWithFormat:_planeImageFormat, kImageShadow];
    
    // Create the plane node
    self.plane = [SKSpriteNode spriteNodeWithImageNamed:planeImage];
    _plane.scale = 0.6;
    _plane.zPosition = 2;
    _plane.position = CGPointMake(0, 0);
    [self addChild:_plane];
    
    // Physics
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_plane.size];
    self.physicsBody.dynamic = YES;
    self.physicsBody.collisionBitMask = 0;
    
    _size = _plane.size;
    _width = _plane.size.width;
    _height = _plane.size.height;
    
    // Create the plane shadow node
    NSString *shadowPath = [[NSBundle mainBundle] pathForResource:[shadowImage stringByDeletingPathExtension] ofType:@"png"];
    _hasShadow = shadowPath != nil;
    if (_hasShadow) {
        self.planeShadow = [SKSpriteNode spriteNodeWithImageNamed:shadowImage];
        _planeShadow.scale = 0.6;
        _planeShadow.zPosition = 1;
        _planeShadow.position = CGPointMake(kShadowOffsetX, kShadowOffsetY);
        [self addChild:_planeShadow];
    }
    
    // Check if the plane has turning images
    NSString *planeImageLeft = [NSString stringWithFormat:_planeImageFormat, kImageLeft];
    NSString *planeImageRight = [NSString stringWithFormat:_planeImageFormat, kImageRight];
    NSString *planeImageLeftPath = [[NSBundle mainBundle] pathForResource:[planeImageLeft stringByDeletingPathExtension] ofType:@"png"];
    NSString *planeImageRightPath = [[NSBundle mainBundle] pathForResource:[planeImageRight stringByDeletingPathExtension] ofType:@"png"];
    _canTurn = (planeImageLeftPath != nil) && (planeImageRightPath != nil);
    
    // Create the plane propeller node
    self.propeller = [SKSpriteNode spriteNodeWithImageNamed:kImagePropeller1];
    _propeller.scale = 0.2;
    _propeller.zPosition = 2;
    _propeller.position = CGPointMake(0, kPropellerOffsetY);
    [self addChild:_propeller];
    
    // Create the propeller spin animation
    SKAction *propellerAnimation = [self createPropellerAnimation];
    [_propeller runAction:propellerAnimation];
    
    // Create the smoke trail
    NSString *smokePath = [[NSBundle mainBundle] pathForResource:@"AirplaneTrail" ofType:@"sks"];
    self.smokeTrail = [NSKeyedUnarchiver unarchiveObjectWithFile:smokePath];
    _smokeTrail.zPosition = 2;
    _smokeTrail.position = CGPointMake(0, kSmokeTrailOffsetY);
    [self addChild:_smokeTrail];
}

#pragma mark - Static methods

static NSArray *_explosionTextures;

+ (NSArray *)explosionTextures {
    if (_explosionTextures == nil) {
        SKTextureAtlas *explosionAtlas = [SKTextureAtlas atlasNamed:@"Explosion"];
        NSArray *textureNames = [explosionAtlas textureNames];
        NSMutableArray *textures = [[NSMutableArray alloc] initWithCapacity:textureNames.count];
        for (NSString *name in textureNames) {
            SKTexture *texture = [explosionAtlas textureNamed:name];
            [textures addObject:texture];
        }
        _explosionTextures = textures;
    }
    return _explosionTextures;
}

#pragma mark - Public methods

- (void)turn:(IDAirplaneTurnDirection)direction {
    if (!_canTurn) {
        return;
    }
    
    NSString *turnImage = nil;
    CGFloat turnOffset = 0;
    switch (direction) {
        case IDAirplaneTurnLeft:
            turnImage = kImageLeft;
            turnOffset = -5.0;
            break;
        case IDAirplaneTurnRight:
            turnImage = kImageRight;
            turnOffset = 5.0;
            break;
        default:
            turnImage = kImageNormal;
            break;
    }
    
    // Set the plane texture
    NSString *planeImage = [NSString stringWithFormat:_planeImageFormat, turnImage];
    _plane.texture = [SKTexture textureWithImageNamed:planeImage];
    
    // Offset other nodes so they look good when the plane is turned
    _propeller.position = CGPointMake(turnOffset, kPropellerOffsetY);
    _smokeTrail.position = CGPointMake(turnOffset, kSmokeTrailOffsetY);
}

- (SKSpriteNode *)fire {
    // Create a new projectile
    SKSpriteNode *projectile = [SKSpriteNode spriteNodeWithImageNamed:kImageProjectile];
    projectile.position = self.position;
    projectile.zPosition = 1;
    projectile.scale = 0.8;
    
    // Physics
    projectile.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:projectile.size];
    projectile.physicsBody.dynamic = NO;
    if (_isPlayer) {
        projectile.physicsBody.categoryBitMask = IDCollisionBodyPlayerProjectile;
        projectile.physicsBody.contactTestBitMask = IDCollisionBodyEnemy;
    } else {
        projectile.physicsBody.categoryBitMask = IDCollisionBodyEnemyProjectile;
        projectile.physicsBody.contactTestBitMask = IDCollisionBodyPlayer;
    }
    projectile.physicsBody.collisionBitMask = 0;
    
    SKAction *travel = [SKAction moveToY:[[UIScreen mainScreen] bounds].size.height + projectile.size.height duration:2];
    SKAction *remove = [SKAction removeFromParent];
    
    [projectile runAction:[SKAction sequence:@[travel, remove]]];
    return projectile;
}

- (void)explode {
    NSArray *explosionTextures = [IDAirplane explosionTextures];
    self.physicsBody = nil;
    
    SKSpriteNode *explosion = [SKSpriteNode spriteNodeWithTexture:[explosionTextures objectAtIndex:0]];
    explosion.scale = 0.6;
    explosion.zPosition = 2;
    [self addChild:explosion];
    
    SKAction *animation = [SKAction animateWithTextures:_explosionTextures timePerFrame:0.07];
    SKAction *remove = [SKAction removeFromParent];
    [self removeActionForKey:kEnemyPlaneMovementAction];
    [explosion runAction:[SKAction sequence:@[animation, remove]] completion:^{
        [self runAction:remove];
    }];
}

- (void)setIsPlayer:(BOOL)isPlayer {
    _isPlayer = isPlayer;
    if (isPlayer) {
        self.physicsBody.categoryBitMask = IDCollisionBodyPlayer;
        self.physicsBody.contactTestBitMask = IDCollisionBodyEnemyProjectile;
    } else {
        self.physicsBody.categoryBitMask = IDCollisionBodyEnemy;
        self.physicsBody.contactTestBitMask = IDCollisionBodyPlayerProjectile;
    }
}

#pragma mark - Private methods

- (NSString *)imageFromModel:(IDAirplaneModel)model {
    switch (model) {
        case IDAirplaneModel1:
            return @"PLANE_1_%@.png";
            break;
        case IDAirplaneModel2:
            return @"PLANE_2_%@.png";
            break;
        case IDAirplaneModel8:
            return @"PLANE_8_%@.png";
            break;
        default:
            return @"PLANE_8_%@.png";
            break;
    }
}

- (SKAction *)createPropellerAnimation {
    SKTexture *propeller1 = [SKTexture textureWithImageNamed:kImagePropeller1];
    SKTexture *propeller2 = [SKTexture textureWithImageNamed:kImagePropeller2];
    SKAction *spin = [SKAction animateWithTextures:@[propeller1, propeller2] timePerFrame:0.1];
    SKAction *spinForever = [SKAction repeatActionForever:spin];
    return spinForever;
}

@end
