//
//  IDGenerator.h
//  Airplanes
//
//  Created by Ivan Fabijanovic on 11/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "IDAirplane.h"

@protocol IDGeneratorDelegate <NSObject>

- (void)addChild:(SKNode *)node;
- (void)runAction:(SKAction *)action withKey:(NSString *)key;
- (void)removeActionForKey:(NSString *)key;

@end

@interface IDGenerator : NSObject

- (id)init;

- (void)start;
- (void)stop;

@property (nonatomic, weak) id<IDGeneratorDelegate> delegate;

@property (nonatomic, assign) NSTimeInterval interval;
@property (nonatomic, assign) BOOL generateEnemies;

@end
