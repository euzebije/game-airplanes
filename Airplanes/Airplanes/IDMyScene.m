//
//  IDMyScene.m
//  Airplanes
//
//  Created by Ivan Fabijanovic on 10/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import "IDMyScene.h"

#define kHorizontalSensitivity  0.05
#define kVerticalSensitivity    0.05

@implementation IDMyScene {
    IDGenerator *_generator;
    IDCollisionManager *_collisionManager;
}

-(id)initWithSize:(CGSize)size {
    self = [super initWithSize:size];
    if (self) {
        screenRect = [[UIScreen mainScreen] bounds];
        screenHeight = screenRect.size.height;
        screenWidth = screenRect.size.width;
        
        self.player = [[IDAirplane alloc] initWithModel:IDAirplaneModel8];
        _player.isPlayer = YES;
        _player.position = CGPointMake(screenWidth/2.0, 15.0 + _player.height);
        [self addChild:_player];
        
        SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"airPlanesBackground"];
        background.position = CGPointMake((CGRectGetMidX(self.frame)), (CGRectGetMidY(self.frame)));
        [self addChild:background];
        
        // Accelerometer
        self.motionManager = [[CMMotionManager alloc] init];
        _motionManager.accelerometerUpdateInterval = 0.2;
        [_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
            [self outputAccelerationData:accelerometerData.acceleration];
            if (error) {
                NSLog(@"[Game] Accelerometer error: %@", error);
            }
        }];

        // Generator
        _generator = [[IDGenerator alloc] init];
        _generator.delegate = self;
        [_generator start];
        
        // Collision manager
        _collisionManager = [[IDCollisionManager alloc] init];
        
        // Physics
        self.physicsWorld.gravity = CGVectorMake(0, 0); // No gravity
        self.physicsWorld.contactDelegate = _collisionManager;
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    SKSpriteNode *projectile = [_player fire];
    [self addChild:projectile];
}

-(void)update:(CFTimeInterval)currentTime {
    float minX = _player.width/2.0;
    float maxX = screenWidth - _player.width/2.0;
    float minY = _player.height/2.0;
    float maxY = screenHeight - _player.height/2.0;
    
    float newX = currentMaxAccelerationX * 10;
    float newY = 6.0 + currentMaxAccelerationY * 10;
    
    if (currentMaxAccelerationX > kHorizontalSensitivity) {
        [_player turn:IDAirplaneTurnRight];
    } else if (currentMaxAccelerationX < -kHorizontalSensitivity) {
        [_player turn:IDAirplaneTurnLeft];
    } else {
        [_player turn:IDAirplaneTurnNormal];
    }
    
    newX = MIN(MAX(newX + _player.position.x, minX), maxX);
    newY = MIN(MAX(newY + _player.position.y, minY), maxY);
    
    _player.position = CGPointMake(newX, newY);
}

#pragma mark - Helper methods

- (void)outputAccelerationData:(CMAcceleration)acceleration {
    currentMaxAccelerationX = 0;
    currentMaxAccelerationY = 0;
    
    if (fabs(acceleration.x) > fabs(currentMaxAccelerationX)) {
        currentMaxAccelerationX = acceleration.x;
    }
    if (fabs(acceleration.y) > fabs(currentMaxAccelerationY)) {
        currentMaxAccelerationY = acceleration.y;
    }
}

@end
